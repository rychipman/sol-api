package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
)

func (app *App) handleGetData(w http.ResponseWriter, r *http.Request) {
	app.Logf("sending made-up readings data")

	readings := []float64{}
	numReadings := rand.Intn(10) + 5
	for i := 0; i < numReadings; i++ {
		num := rand.Float64() * 10
		readings = append(readings, num)
	}

	res := map[string]interface{}{
		"readings": readings,
	}

	writeData(w, res)
}

func (app *App) handleGetSensors(w http.ResponseWriter, r *http.Request) {
	app.Logf("sending made-up sensor list")

	res := map[string]interface{}{
		"sensors": []interface{}{
			"asidfb",
			"alsdkfjaslkdfj",
			"alskdfjasldkj",
			"al;dkfjas;dklfj",
		},
	}

	writeData(w, res)
}

func (app *App) handleRegisterDevice(w http.ResponseWriter, r *http.Request) {
	app.Logf("handleRegisterDevice not implemented")
	writeError(w, fmt.Errorf("handleRegisterDevice not implemented"))
}

func (app *App) handleRegisterUser(w http.ResponseWriter, r *http.Request) {

	params := struct {
		Email, Password string
	}{}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&params)
	if err != nil {
		app.Logw("failed to decode request body", "error", err)
		writeError(w, err)
		return
	}

	err = app.CreateUser(params.Email, params.Password)
	if err != nil {
		app.Logw("user creation failed", "error", err)
		writeError(w, err)
		return
	}

	app.Logw("user creation successful", "email", params.Email)

	res := map[string]interface{}{
		"email": params.Email,
	}
	writeData(w, res)
}

func (app *App) handleLogin(w http.ResponseWriter, r *http.Request) {

	params := struct {
		Email, Password string
	}{}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&params)
	if err != nil {
		app.Logw("failed to decode request body", "error", err)
		writeError(w, err)
		return
	}

	token, err := app.LoginUser(params.Email, params.Password)
	if err != nil {
		app.Logw("login failed", "error", err)
		writeError(w, err)
		return
	}

	app.Logw("login successful", "email", params.Email)

	res := map[string]interface{}{
		"email": params.Email,
		"token": token,
	}
	writeData(w, res)
}
