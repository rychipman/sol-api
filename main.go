package main

import (
	"flag"
	"fmt"
	"net/http"

	"go.uber.org/zap"
)

var (
	host     = flag.String("host", "localhost", "address on which to serve the API (default: localhost)")
	port     = flag.Int("port", 8080, "port on which to serve the API (default: 8080)")
	mongoURI = flag.String("mongo", "mongodb://localhost:27017", "MongoDB URI (default: mongodb://localhost:27017)")
	dbName   = flag.String("db", "sol", "MongoDB database name (default: sol)")
)

func main() {
	unsugared, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	lg := unsugared.Sugar()

	flag.Parse()

	lg.Infow("parsed configuration flags",
		"host", *host,
		"port", *port,
		"mongoURI", *mongoURI,
		"dbName", *dbName,
	)

	db, err := connectMongoDB()
	if err != nil {
		lg.Fatal(err)
	}

	app := newApp(lg, db)

	handle(
		"/user/register",
		app.handleRegisterUser,
		app.withTracing,
		app.withCors,
	)

	handle(
		"/user/login",
		app.handleLogin,
		app.withTracing,
		app.withCors,
	)

	handle(
		"/device/register",
		app.handleRegisterDevice,
		app.withTracing,
		app.withCors,
		app.withUserAuth,
	)

	handle(
		"/sensors",
		app.handleGetSensors,
		app.withTracing,
		app.withCors,
		app.withUserAuth,
	)

	handle(
		"/data",
		app.handleGetData,
		app.withTracing,
		app.withCors,
		app.withUserAuth,
	)

	addr := fmt.Sprintf("%s:%d", *host, *port)
	lg.Infow("serving sol-api", "address", addr)
	lg.Fatal(http.ListenAndServe(addr, nil))
}

func handle(route string, handler http.HandlerFunc, mw ...middleware) {
	withMiddleware := chainMiddleware(mw...)
	http.HandleFunc(route, withMiddleware(handler))
}
