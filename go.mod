module github.com/solsensor/sol-api

require (
	github.com/go-stack/stack v1.7.0
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db
	github.com/mongodb/mongo-go-driver v0.0.8
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.8.0
	golang.org/x/crypto v0.0.0-20180621125126-a49355c7e3f8
	golang.org/x/net v0.0.0-20180621144259-afe8f62b1d6b
	golang.org/x/sync v0.0.0-20180314180146-1d60e4601c6f
)
