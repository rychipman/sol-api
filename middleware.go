package main

import (
	"fmt"
	"net/http"
)

type middleware func(http.Handler) http.HandlerFunc

func chainMiddleware(mw ...middleware) middleware {
	return func(next http.Handler) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			last := next
			for i := len(mw) - 1; i >= 0; i-- {
				last = mw[i](last)
			}
			last.ServeHTTP(w, r)
		}
	}
}

// middleware for tracing requests
func (app *App) withTracing(next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		app.Logw("received request", "method", r.Method, "uri", r.RequestURI)
		next.ServeHTTP(w, r)
	}
}

func (app *App) withCors(next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET,POST")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type,Origin,Accept,X-Api-Token")
		if r.Method != "OPTIONS" {
			next.ServeHTTP(w, r)
		}
	}
}

func (app *App) withUserAuth(next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-Api-Token")
		if token == "" {
			app.Logf("request has no api token, rejecting")
			writeError(w, fmt.Errorf("no api token found"))
			return
		}

		err := app.VerifyToken(token)
		if err != nil {
			app.Logw("api token invalid, rejecting", "error", err.Error())
			writeError(w, fmt.Errorf("api token invalid: %v", err))
			return
		}

		next.ServeHTTP(w, r)
	}
}
