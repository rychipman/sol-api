package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/mongodb/mongo-go-driver/mongo"
)

func generateToken() (string, error) {
	b := make([]byte, 16)

	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), nil
}
func writeData(w http.ResponseWriter, data interface{}) {
	encoder := json.NewEncoder(w)
	_ = encoder.Encode(map[string]interface{}{
		"success": true,
		"data":    data,
	})
}

func writeError(w http.ResponseWriter, err error) {
	encoder := json.NewEncoder(w)
	_ = encoder.Encode(map[string]interface{}{
		"success": false,
		"error":   err.Error(),
	})
}

func connectMongoDB() (*mongo.Database, error) {
	client, err := mongo.NewClient(*mongoURI)
	if err != nil {
		return nil, fmt.Errorf("failed to create mongo client: %v", err)
	}

	err = client.Connect(context.TODO())
	if err != nil {
		log.Fatalf("failed to connect mongo client: %v", err)
	}

	return client.Database(*dbName), nil
}
