package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/objectid"
	"github.com/mongodb/mongo-go-driver/mongo"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

type App struct {
	db *mongo.Database
	lg *zap.SugaredLogger
}

func newApp(lg *zap.SugaredLogger, db *mongo.Database) *App {
	return &App{
		db: db,
		lg: lg,
	}
}

func (app *App) Logf(format string, args ...interface{}) {
	app.lg.Debugf(format, args...)
}

func (app *App) Logw(msg string, keysAndVals ...interface{}) {
	app.lg.Debugw(msg, keysAndVals...)
}

func (app *App) LoginUser(email, password string) (string, error) {
	ctx := context.Background()

	filter := map[string]string{
		"email": email,
	}

	res := struct {
		UserID         objectid.ObjectID `bson:"_id"`
		Email          string
		HashedPassword string `bson:"password"`
	}{}

	err := app.db.Collection("users").FindOne(ctx, filter).Decode(&res)
	if err != nil {
		return "", fmt.Errorf("failed to find user with email '%s': %v", email, err)
	}

	hashBytes := []byte(res.HashedPassword)
	pwdBytes := []byte(password)
	err = bcrypt.CompareHashAndPassword(hashBytes, pwdBytes)
	if err != nil {
		return "", err
	}

	return app.CreateUserToken(res.UserID)
}

func (app *App) CreateUser(email, password string) error {
	if email == "" {
		return fmt.Errorf("email address may not be empty")
	}
	email = strings.ToLower(email)

	if len(password) < 10 {
		return fmt.Errorf("password must be at least ten characters")
	}

	hashBytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return fmt.Errorf("failed to hash password: %v", err)
	}

	doc := map[string]string{
		"email":    email,
		"password": string(hashBytes),
	}

	ctx := context.Background()
	_, err = app.db.Collection("users").InsertOne(ctx, doc)
	if err != nil {
		return fmt.Errorf("failed to create user: %v", err)
	}

	return nil
}

// Get a list of registered users.
func (app *App) Users() ([]string, error) {
	users := []string{}
	ctx := context.Background()

	cur, err := app.db.Collection("users").Find(ctx, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to find users: %v\n", err)
	}

	for cur.Next(ctx) {
		elem := bson.NewDocument()
		if err := cur.Decode(elem); err != nil {
			return nil, fmt.Errorf("failed to decode document: %v", err)
		}

		email, ok := elem.Lookup("email").StringValueOK()
		if !ok {
			return nil, fmt.Errorf("failed to find field 'email' in user doc")
		}

		users = append(users, email)
	}

	if err := cur.Err(); err != nil {
		return nil, fmt.Errorf("cursor error: %v", err)
	}

	return users, nil
}

func (app *App) CreateUserToken(userID objectid.ObjectID) (string, error) {
	token, err := generateToken()
	if err != nil {
		return "", fmt.Errorf("failed to generate token: %v", err)
	}

	doc := map[string]interface{}{
		"token":   token,
		"type":    "user",
		"user_id": userID,
	}

	ctx := context.Background()
	_, err = app.db.Collection("tokens").InsertOne(ctx, doc)
	if err != nil {
		return "", fmt.Errorf("failed to insert token into db: %v", err)
	}

	return token, nil
}

func (app *App) VerifyToken(token string) error {
	ctx := context.Background()

	filter := map[string]string{
		"token": token,
	}

	res := struct {
		Token  string
		UserID objectid.ObjectID `bson:"user_id"`
	}{}

	return app.db.Collection("tokens").FindOne(ctx, filter).Decode(&res)
}
